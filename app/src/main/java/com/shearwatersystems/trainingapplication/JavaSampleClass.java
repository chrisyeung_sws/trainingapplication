package com.shearwatersystems.trainingapplication;

public class JavaSampleClass {
    private String javaVariable;
    
    public JavaSampleClass(String javaVariable) {
        this.javaVariable = javaVariable;
    }
    
    public String javaMethod(String prefix) {
        return javaVariable + prefix;
    }
}
