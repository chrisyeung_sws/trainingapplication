package com.shearwatersystems.trainingapplication

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.TypeFactory
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.item_allergy.view.*
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {
    var selectedData: Allergy? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        supportFragmentManager.beginTransaction().replace(R.id.flList, ListFragment()).commit()
    }
    
    fun onDataSelected(data: Allergy) {
        selectedData = data
        supportFragmentManager.beginTransaction().addToBackStack(null).replace(R.id.flList, DetailFragment()).commit()
    }
}

class DetailFragment : Fragment() {
    private val data by lazy { (activity as MainActivity).selectedData!! }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etAllergen.setText(data.allergen)
        etAllergyResult.setText(data.allergyResult)
        etNote.setText(data.note)
        etDateRecorded.setText(data.dateRecorded)
    }
}

class ListFragment : Fragment() {
    private lateinit var allergyList: ArrayList<Allergy>
    private val listAdaptor by lazy { ListAdaptor() }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadListItems()
    }
    
    private fun loadListItems() {
        thread {
            activity?.apply {
                allergyList = ObjectMapper().readValue<ArrayList<Allergy>>(assets.open("sample.json"),
                        TypeFactory.defaultInstance().constructCollectionType(ArrayList::class.java, Allergy::class.java))
                
                runOnUiThread { createList() }
            }
        }
    }
    
    private fun createList() {
        rvList.layoutManager = LinearLayoutManager(context)
        rvList.adapter = listAdaptor
    }
    
    inner class ListItemVH(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var data: Allergy? = null
        
        init {
            itemView.llBase.setOnClickListener(this)
        }
        
        fun bindData(allergy: Allergy) {
            this.data = allergy
            itemView.tvAllergen.text = allergy.allergen
            itemView.tvAllergyResult.text = allergy.allergyResult
        }
        
        override fun onClick(v: View) {
            data?.let {
                (activity as? MainActivity)?.onDataSelected(it)
            }
        }
    }
    
    inner class ListAdaptor : RecyclerView.Adapter<ListItemVH>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemVH =
                ListItemVH(LayoutInflater.from(context).inflate(R.layout.item_allergy, parent, false))
        
        override fun getItemCount(): Int = allergyList.size
        
        override fun onBindViewHolder(holder: ListItemVH, position: Int) = holder.bindData(allergyList[position])
        
    }
}

class Allergy {
    @JsonProperty("Allergen")
    var allergen: String? = null
    
    @JsonProperty("AllergyResult")
    var allergyResult: String? = null
    
    @JsonProperty("Note")
    var note: String? = null
    
    @JsonProperty("DateRecorded")
    var dateRecorded: String? = null
}