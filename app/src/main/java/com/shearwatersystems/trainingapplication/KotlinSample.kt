package com.shearwatersystems.trainingapplication

open class KotlinSample( //primary constructor
        // constructor variables:
        val readableProperty: String,
        var writableProperty: String,
        constructorVariable: String
) {
    // define static properties and functions:
    companion object {
        var classVariable: String = "test"
        
        const val CONSTANT: String = "this is a constant" // const variable will be replaced inline during compile time
    }
    
    init {
        writableProperty = constructorVariable + "test"
    }
    
    //second constructor
    constructor(list: List<String>) : this(list[0], list[1], list[2])
    
    private var privateProperty: Int = 0
    open var publicProperty: Long = 0L
    
    private var nullableProperty: Float? = null
    // Compiler error when assign a null value to nonnull property:
    // private var nonnullProperty: Float = null
    
    //Use "by" modifier to set the getter/setter delegation of the property
    //one of the very useful delegation is "lazy". It allow property to be initialised only when used.
    private val lazyProperty by lazy { "some resources only available after certain state, e.g. Activity.onCreate()" }
    
    //function (method)
    open fun publicFunction(param: String): String {
        print(param)
        return "echo" + param
    }
    
    private fun privateFunction() {
        print("this function can only be called in this class")
    }
    
    //function programming
    private fun functionWithFunctionalParameter(num1: Int, num2: Int, callback: (a: Int, b: Int) -> Int): Int {
        return callback.invoke(num1, num2)
    }
    
    //using a functional parameter
    private fun useFunctional() {
        var result = functionWithFunctionalParameter(1, 2, { a, b -> a + b })
        // result = 3
        
        // special syntax for the last functional parameter
        result = functionWithFunctionalParameter(2, 4) { a, b ->
            a * b
        }
        // result = 8
    }
    
    //using "stream" like koltin helper function for collections
    private fun useStream() {
        val list = listOf(1, 2, 3, 4, 5)
        
        var result = list.filter { it > 3 } //special syntax "it" for single functional parameter
        // result = [4,5]
        
        result = list.map { it + 3 }
        // result = [4,5,6,7,8]
        
        var result2 = list.reduce { acc, i -> acc + i } // sum all the entries in the list
        // result = 15
    }
    
    
    //static inner class
    class StaticInnerClass {
        // static inner can access class variable, but not instance variables
        var a: String = classVariable
        // var b: Long = publicProperty //error
    }
    
    //inner class
    inner class InnerClass {
        // inner can access both class variable and instance variables
        var a: String = classVariable
        var b: Long = publicProperty
    }
}

//class extension
class ExtendedClass(readableProperty: String,
                    writableProperty: String,
                    constructorVariable: String,
                    var additionalProperty: String
) : KotlinSample(readableProperty, writableProperty, constructorVariable + additionalProperty) {
    //override function
    override fun publicFunction(param: String): String {
        return super.publicFunction(param) + "overridden"
    }
    
    //override property
    override var publicProperty: Long = 0L
        get() = field + 1 //override getter
        set(value) { //override setter
            field = value - 1
        }
}

open class KotlinSampleClass : JavaSampleClass("Kotlin") { //kotlin extend a java class
    fun test(prefix: String): String {
        val result = javaMethod("Test") //kotlin calling java method
        //result = "KotlinTest"
        
        return prefix + result!! //java does not have null checking, use !! to assert the variable is always nonnull
    }
}